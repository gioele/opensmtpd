Source: opensmtpd
Section: mail
Priority: optional
Maintainer: Ryan Kavanagh <rak@debian.org>
Build-Depends: debhelper-compat (= 13)
 , bison
 , libdb-dev
 , libevent-dev
 , libpam0g-dev
 , libssl-dev
 , po-debconf
 , zlib1g-dev
Rules-Requires-Root: binary-targets
Standards-Version: 4.6.0
Homepage: https://www.opensmtpd.org/
Vcs-Git: https://salsa.debian.org/debian/opensmtpd.git -b debian/sid
Vcs-Browser: https://salsa.debian.org/debian/opensmtpd

Package: opensmtpd
Architecture: any
Provides: mail-transport-agent
Conflicts: mail-transport-agent
 , sendmail-base
Replaces: mail-transport-agent
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
 , adduser
 , ed
 , lsb-base (>= 3.0-6)
Recommends: opensmtpd-extras
Breaks: opensmtpd-extras (<< 6.6.0)
Suggests: ca-certificates
Description: secure, reliable, lean, and easy-to configure SMTP server
 The OpenSMTPD server seeks to be
  * as secure as possible, and uses privilege separation to mitigate
    possible security bugs
  * as reliable as possible: any accepted email must not be lost
  * lean: it covers typical usage cases instead of every obscure one
  * easy to configure, with a configuration syntax reminiscent of the OpenBSD
    Packet Filter's (PF)
  * fast and efficient: it can handle large queues with reasonable performance
